﻿using GTA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace AmphibiousVehicles
{
    internal class AmphibianVehicle
    {
        public Model Model;
        public float Height;

        public AmphibianVehicle(Model model, float height)
        {
            Model = model;
            Height = height;
        }
    }

    public class AmphibiousVehicles : Script
    {
        private bool useHoldKey;
        private Keys holdKey, pressKey, changeHeightKey, addHeightKey, subHeightKey;

        private bool enabled = true, changingHeight = false;
        private Model[] platformModels = new Model[] { "BM_Contnr_01a", "nj05_oiltnkbig" };
        private GTA.Object floatingObj;
        private List<AmphibianVehicle> amphibiousVehicles = new List<AmphibianVehicle>();

        private void Subtitle(string text, int time = 1500)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", text, time, 1);
        }

        private bool isInGame()
        {
            if (Game.Exists(Player.Character) && Player.Character.isAlive && GTA.Native.Function.Call<bool>("IS_SCREEN_FADED_IN") && !GTA.Native.Function.Call<bool>("IS_SCREEN_FADED_OUT") && !GTA.Native.Function.Call<bool>("IS_SCREEN_FADING_IN")) return true;
            return false;
        }

        private float GetAmphibiousModelHeight(Model model)
        {
            return amphibiousVehicles.Find(amph => amph.Model == model).Height;
        }

        private bool isModelAmphibious(Model model)
        {
            foreach (AmphibianVehicle a in amphibiousVehicles)
                if (a.Model == model)
                    return true;
            return false;
        }

        private GTA.Object CreateTheObject(Vector3 dimensions)
        {
            Model model = platformModels[1];
            Vector3 lastObjDimensions = new Vector3(999, 999, 999);

            for (int i = 1; i < platformModels.Length; i++)
            {
                Vector3 objDimensions = platformModels[i].GetDimensions();
                if (objDimensions.X > dimensions.X && objDimensions.Y > dimensions.Y && objDimensions.X < lastObjDimensions.X && objDimensions.Y < lastObjDimensions.Y)
                {
                    model = platformModels[i];
                }
            }

            Game.Console.Print(model.ToString());

            GTA.Object tempObj = World.CreateObject(model, Vector3.Zero);
            while (!Game.Exists(tempObj)) Wait(50);
            tempObj.FreezePosition = true;

            return tempObj;
        }

        private void LoadSettings()
        {
            useHoldKey = Settings.GetValueBool("use_toggle_mod_hold_key", "keys", true);
            holdKey = Settings.GetValueKey("toggle_mod_hold_key", "keys", Keys.RControlKey);
            pressKey = Settings.GetValueKey("toggle_mod_press_key", "keys", Keys.A);
            changeHeightKey = Settings.GetValueKey("start_change_height_key", "keys", Keys.F7);
            addHeightKey = Settings.GetValueKey("increase_height_key", "keys", Keys.Add);
            subHeightKey = Settings.GetValueKey("decrease_height_key", "keys", Keys.Subtract);

            if (File.Exists(@".\scripts\amph_vehs.ini"))
            {
                if (amphibiousVehicles.Count > 0) amphibiousVehicles.Clear();

                using (StreamReader sr = new StreamReader(@".\scripts\amph_vehs.ini"))
                {
                    string line;

                    while (sr.Peek() >= 0)
                    {
                        line = sr.ReadLine();

                        if (string.IsNullOrWhiteSpace(line)) continue;

                        if (line.Contains('#')) line = line.Remove(line.IndexOf('#'));

                        if (!line.Contains(',')) continue;

                        string[] lineSplit = line.Split(',');

                        if (string.IsNullOrWhiteSpace(lineSplit[0]) || string.IsNullOrWhiteSpace(lineSplit[1])) continue;

                        float value;
                        bool result = float.TryParse(lineSplit[1].Trim(), out value);

                        if (result)
                        {
                            amphibiousVehicles.Add(new AmphibianVehicle(lineSplit[0].Trim(), value));
                        }
                    }

                    sr.Close();
                }
            }
        }

        private float tempHeight = -9.5f;

        private void AmphibiousVehicles_Tick(object sender, EventArgs e)
        {
            if (enabled)
            {
                if (Player.Character.isInVehicle() && isModelAmphibious(Player.Character.CurrentVehicle.Model))
                {
                    Vector3 vehDimensions = Player.Character.CurrentVehicle.Model.GetDimensions();
                    Vector3 objDimensions = floatingObj.Model.GetDimensions();

                    if (!Game.Exists(floatingObj)) floatingObj = CreateTheObject(vehDimensions);
                    else if (vehDimensions.X > objDimensions.X || vehDimensions.Y > objDimensions.Y)
                    {
                        floatingObj.Delete();
                        floatingObj = CreateTheObject(vehDimensions);
                    }

                    Vector3 playerPos = Player.Character.Position, posToPlaceObj = new Vector3(playerPos.X, playerPos.Y, ((floatingObj.Model == platformModels[0]) ? 6.6f : 0f) + (changingHeight ? tempHeight : GetAmphibiousModelHeight(Player.Character.CurrentVehicle.Model)));
                    if (Math.Abs(playerPos.Z - posToPlaceObj.Z) < 50.0f)
                    {
                        floatingObj.Collision = true;
                        floatingObj.Position = posToPlaceObj;
                        floatingObj.Heading = Player.Character.CurrentVehicle.Heading;
                    }
                    else
                    {
                        floatingObj.Collision = false;
                    }
                }

                if (changingHeight)
                {
                    if (Player.Character.isInVehicle())
                    {
                        Subtitle(string.Format("Height: {0}. Press Enter to save or Backspace to stop without saving", tempHeight), 350);

                        if (isKeyPressed(addHeightKey))
                        {
                            tempHeight = (float)Math.Round((decimal)(tempHeight + 0.05f), 2, MidpointRounding.ToEven);
                        }
                        else if (isKeyPressed(subHeightKey))
                        {
                            tempHeight = (float)Math.Round((decimal)(tempHeight - 0.05f), 2, MidpointRounding.ToEven);
                        }
                    }
                    else
                    {
                        changingHeight = false;
                        tempHeight = -9.5f;
                    }
                }
            }
        }

        private void AmphibiousVehicles_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if ((!useHoldKey || isKeyPressed(holdKey)) && e.Key == pressKey)
            {
                Subtitle("AmphibiousVehicles is now " + ((enabled = !enabled) ? "enabled" : "disabled"));
                if (!enabled) floatingObj.Collision = false;
            }

            if (enabled)
            {
                if (e.Key == changeHeightKey)
                {
                    if (!enabled && !Player.Character.isInVehicle()) return;

                    changingHeight = !changingHeight;
                    if (!changingHeight) Subtitle("Stopped changing of height for current model. If you changed the value it has been saved");
                }

                if (changingHeight)
                {
                    if (e.Key == Keys.Enter)
                    {
                        string model = string.Empty;
                        string toWrite = string.Empty;
                        List<string> lines = new List<string>();

                        using (StreamReader sr = new StreamReader(@".\scripts\amph_vehs.ini"))
                        {
                            string backLine, frontLine;

                            while (sr.Peek() >= 0)
                            {
                                backLine = frontLine = sr.ReadLine();

                                lines.Add(frontLine);

                                if (string.IsNullOrWhiteSpace(frontLine)) continue;

                                if (frontLine.Contains('#')) frontLine = frontLine.Remove(frontLine.IndexOf('#'));

                                if (!frontLine.Contains(',')) continue;

                                string[] lineSplit = frontLine.Split(',');

                                if (string.IsNullOrWhiteSpace(lineSplit[0]) || string.IsNullOrWhiteSpace(lineSplit[1]) || new Model(lineSplit[0].Trim()) != Player.Character.CurrentVehicle.Model) continue;

                                model = lineSplit[0].Trim();

                                toWrite = Regex.Replace(backLine, ",(.*?)" + lineSplit[1].Trim(), "," + tempHeight.ToString());
                            }

                            sr.Close();
                        }

                        if (model != string.Empty)
                        {
                            using (TextWriter tw = new StreamWriter(@".\scripts\amph_vehs.ini", false))
                            {
                                foreach (string line in lines)
                                {
                                    if (line.Contains(model)) tw.WriteLine(toWrite);
                                    else tw.WriteLine(line);
                                }
                                tw.Flush();
                                tw.Close();
                            }

                            amphibiousVehicles.First(i => i.Model == model).Height = tempHeight;

                            //LoadSettings();
                        }

                        changingHeight = false;
                        tempHeight = -9.5f;
                    }
                    else if (e.Key == Keys.Back)
                    {
                        changingHeight = false;
                        tempHeight = -9.5f;
                    }
                }
            }
        }

        public AmphibiousVehicles()
        {
            if (!File.Exists(Settings.Filename))
            {
                Settings.SetValue("use_toggle_mod_hold_key", "keys", true);
                Settings.SetValue("toggle_mod_hold_key", "keys", Keys.RControlKey);
                Settings.SetValue("toggle_mod_press_key", "keys", Keys.A);
                Settings.SetValue("start_change_height_key", "keys", Keys.F7);
                Settings.SetValue("increase_height_key", "keys", Keys.Add);
                Settings.SetValue("decrease_height_key", "keys", Keys.Subtract);
                Settings.Save();
            }

            LoadSettings();

            if (amphibiousVehicles.Count > 0)
            {
                while (!isInGame()) Wait(500);
                
                floatingObj = CreateTheObject(Vector3.Zero);

                floatingObj.Visible = false;
                floatingObj.Collision = false;

                KeyDown += AmphibiousVehicles_KeyDown;
                Interval = 50;
                Tick += AmphibiousVehicles_Tick;
            }
            else
            {
                string temp = "(AmphibiousVehicles): You did not enter any vehicle models and their float height value into the amph_vehs.ini file";
                Game.Console.Print(temp);
                Game.DisplayText(temp);
            }
        }
    }
}
