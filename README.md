# AmphibiousVehicles
AmphibiousVehicles is a script that can make any vehicle amphibious. 

Requirements:
* An ASI loader (YAASIL recommended)
* ScriptHook
* ScriptHook .NET
* .NET Framework 4.0

[Screenshots](https://imgur.com/a/vicSV)